/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;
import net.codejava.storeproject.poc.TestSelectProduct;

/**
 *
 * @author pc
 */
public class ProductDao implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        //insert data
        try {
            String sql = "INSERT INTO product (name,price )VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //read data
        try {
            String sql = "SELECT id, name,price FROM product";
            Statement stmt = conn.createStatement();
            ResultSet resul = stmt.executeQuery(sql);
            while (resul.next()) {
                int id = resul.getInt("id");
                String name = resul.getString("name");
                double price = resul.getDouble("price");
                Product product = new Product(id, name, price);
                list.add(product);

            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //read data
        try {
            String sql = "SELECT id, name,price FROM product WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet resul = stmt.executeQuery(sql);
            if (resul.next()) {
                int pid = resul.getInt("id");
                String name = resul.getString("name");
                double price = resul.getDouble("price");
                Product product = new Product(pid, name, price);
                return product;

            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        //Delete data
        try {
            String sql = "DELETE FROM product WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return row;
    }

    @Override
    public int update(Product object) {
       Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        int row = 0;

        try {
            String sql = "UPDATE product SET name = ?,price = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return row;
    }

    public static void main(String[] args) {
        ProductDao dao = new ProductDao();
       // System.out.println(dao.getAll());
       // System.out.println(dao.get(1));
        int id = dao.add(new Product(-1, "Kafaeyen", 50.0));
        System.out.println("id: " + id);
        Product lastProduct = dao.get(id);
        lastProduct.setPrice(100);
        //System.out.println("lastProduct: " + lastProduct);
        dao.update(lastProduct);
        Product updateProduct = dao.get(id);
        System.out.println("updateProduct: " + updateProduct);
        dao.delete(id);
        Product deleteProduct = dao.get(id);
        System.out.println("deleteProduct: " + deleteProduct);

    }

}
